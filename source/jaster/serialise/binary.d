module jaster.serialise.binary;

private
{
    import std.exception, std.traits, std.bitmanip, std.utf, std.algorithm, std.array, std.range, std.typecons;
    import jaster.serialise.archive, jaster.serialise.serialiser;
    import jaster.stream;
}

/++
 + Binary format:
 +  'JSE', 
 +  int CRC32 (of all the bytes past this) [Big Endian], 
 +  Object Root (Note that the root is special, and doesn't write out a 'Type' field nor a 'Name' field)
 +
 + Entry Header:
 +  ubyte Type
 +  ubyte[Varies] TypeSpecificData
 +
 + Numeric Entry Data:
 +  T Number [Big Endian]
 +
 + Array Entry Data:
 +  Length Length [Big Endian]
 +  ubyte[Length] Data
 +
 + Attribute Entry Data:
 +  string(Array) Name
 +  Entry Value
 +
 + Object Entry Data:
 +  int Length (of 'Entries' + 'Name' in bytes)
 +  string(Array) Name
 +  Entry[] Entries (includes children, values, and attributes. Always uses an int for the length)
 + ++/
final class ArchiveBinary : Archive
{
    const MAGIC_NUMBER = cast(const ubyte[])"JSE";

    private
    {
        alias IsRoot        = Flag!"isRoot";
        alias WriteLength   = Flag!"entryLength";

        enum DataType : ubyte
        {
            Bool,
            Null,
            UByteArray,
            ValueArray,
            Byte,
            UByte,
            Short,
            UShort,
            Int,
            UInt,
            Long,
            ULong,
            String,
            Float,
            Double,

            Object = 0xFE,
            Attribute = 0xFF
        }

        ArchiveObject _obj;
    }

    this()
    {
        this._obj = new ArchiveObject();
    }

    public override
    {
        const(ubyte[]) saveToMemory()
        {
            import std.digest.crc : crc32Of;

            auto stream = new BinaryIO(new MemoryStreamGC());

            stream.writeBytes(MAGIC_NUMBER);
            stream.write!int(0); // CRC reserved

            auto start = stream.stream.position;
            this.saveObject(stream, this.root, IsRoot.yes);
            auto length = (stream.stream.position - start);

            stream.stream.position = start;
            auto bytes = stream.readBytes(length);
            stream.stream.position = start - int.sizeof;
            auto crc = crc32Of(bytes); // According to the docs, this is little-endian
            stream.write!int((cast(int[])crc)[0]);

            stream.stream.position = 0;
            return stream.readBytes(stream.stream.length);
        }

        void loadFromMemory(const ubyte[] data)
        {
            import std.digest.crc : crc32Of;
            import std.bitmanip   : swapEndian;

            auto stream = new BinaryIO(new MemoryStreamGC());
            stream.writeBytes(data.dup);
            stream.stream.position = 0;
            enforce(stream.readBytes(3) == MAGIC_NUMBER, "Invalid Magic number");

            auto crc = stream.read!int();
            version(BigEndian)
                crc = crc.swapEndian; // CRC is apparently always in little-endian

            auto start = stream.stream.position;
            auto length = stream.read!int;

            stream.stream.position = stream.stream.position - int.sizeof;
            enforce(stream.readBytes(length + int.sizeof).crc32Of == cast(ubyte[4])((&crc)[0..1]),
                    "CRC Mismatch. This is usually a sign that the data has been tampered with, or some kind of transport error occured.");

            stream.stream.position = start;
            this._obj = this.loadObject(stream, IsRoot.yes);
        }

        @property
        ArchiveObject root()
        {
            return this._obj;
        }
    }

    // ##########
    // # COMMON #
    // ##########
    private
    {
        DataType getTypeFor(ArchiveValue value)
        {
                 if(value.type == typeid(bool))             return DataType.Bool;
            else if(value.type == typeid(typeof(null)))     return DataType.Null;
            else if(value.type == typeid(ubyte[]))          return DataType.UByteArray;
            else if(value.type == typeid(ArchiveValue[]))   return DataType.ValueArray;
            else if(value.type == typeid(byte))             return DataType.Byte;
            else if(value.type == typeid(ubyte))            return DataType.UByte;
            else if(value.type == typeid(short))            return DataType.Short;
            else if(value.type == typeid(ushort))           return DataType.UShort;
            else if(value.type == typeid(int))              return DataType.Int;
            else if(value.type == typeid(uint))             return DataType.UInt;
            else if(value.type == typeid(long))             return DataType.Long;
            else if(value.type == typeid(ulong))            return DataType.ULong;
            else if(value.type == typeid(string))           return DataType.String;
            else if(value.type == typeid(float))            return DataType.Float;
            else if(value.type == typeid(double))           return DataType.Double;

            assert(false);
        }
    }
    
    // #####################
    // # LOADING FUNCTIONS #
    // #####################
    private
    {
        ArchiveValue loadValue(BinaryIO stream, DataType type)
        {
            switch(type) with(DataType)
            {       
                case Bool:
                    return ArchiveValue(cast(bool)stream.readBytes(1)[0]);

                case Null:
                    return ArchiveValue(null);

                case UByteArray:
                    return ArchiveValue(stream.read!(ubyte[]));

                case Byte:
                    return ArchiveValue(stream.read!byte);

                case UByte:
                    return ArchiveValue(stream.read!ubyte);
                    
                case Short:
                    return ArchiveValue(stream.read!short);
                    
                case UShort:
                    return ArchiveValue(stream.read!ushort);
                    
                case Int:
                    return ArchiveValue(stream.read!int);
                    
                case UInt:
                    return ArchiveValue(stream.read!uint);
                    
                case Long:
                    return ArchiveValue(stream.read!long);
                    
                case ULong:
                    return ArchiveValue(stream.read!ulong);
                    
                case String:
                    return ArchiveValue(stream.read!string);
                    
                case Float:
                    return ArchiveValue(stream.read!float);
                    
                case Double:
                    return ArchiveValue(stream.read!double);
                    
                case ValueArray:
                    auto length = stream.readLengthBytes();
                    ArchiveValue[] values;
                    foreach(i; 0..length)
                    {
                        auto type2 = stream.readBytes(1)[0];
                        values ~= this.loadValue(stream, cast(DataType)type2);
                    }
                    return ArchiveValue(values);

                default:
                    import std.conv : to;
                    assert(false, type.to!string);
            }
        }

        ArchiveObject loadObject(BinaryIO stream, IsRoot isRoot = IsRoot.no)
        {
            import std.conv : to;

            auto length = stream.read!uint;
            auto start  = stream.stream.position;
            auto name   = (isRoot) ? "" : stream.read!string;
            auto obj    = new ArchiveObject(name);

            while(true)
            {
                enforce(stream.stream.position <= start + length, 
                        "Malformed data. Attempted to read past an object's data.");
                
                if(stream.stream.position == start + length)
                    break;

                auto type = stream.readBytes(1)[0];
                switch(type) with(DataType)
                {
                    case cast(ubyte)Bool:
                    case cast(ubyte)Null:
                    case cast(ubyte)UByteArray:
                    case cast(ubyte)ValueArray:
                    case cast(ubyte)Byte:
                    case cast(ubyte)UByte:
                    case cast(ubyte)Short:
                    case cast(ubyte)UShort:
                    case cast(ubyte)Int:
                    case cast(ubyte)UInt:
                    case cast(ubyte)Long:
                    case cast(ubyte)ULong:
                    case cast(ubyte)String:
                    case cast(ubyte)Float:
                    case cast(ubyte)Double:
                        obj.addValue(this.loadValue(stream, cast(DataType)type));
                        break;

                    case cast(ubyte)Object:
                        obj.addChild(this.loadObject(stream));
                        break;

                    case cast(ubyte)Attribute:
                        auto attribName = stream.read!string;
                        obj.setAttribute(attribName, this.loadValue(stream, cast(DataType)stream.readBytes(1)[0]));
                        break;

                    default:
                        throw new Exception("Malformed data. Unknown entry data type: " ~ type.to!string);
                }
            }

            return obj;
        }
    }

    // ####################
    // # SAVING FUNCTIONS #
    // ####################
    private
    {
        void saveEntry(BinaryIO stream, DataType type, void delegate() saver, WriteLength hasLength, IsRoot isRoot = IsRoot.no)
        {
            if(!isRoot)
                stream.write!ubyte(cast(ubyte)type);

            if(hasLength)
                stream.write!int(0); // Length, reserved

            auto start = stream.stream.position;
            saver();

            if(hasLength)
            {
                auto length = (stream.stream.position - start);
                assert(stream.stream.position >= start);

                stream.stream.position = start - int.sizeof;
                stream.write!int(cast(int)length);
                stream.stream.position = stream.stream.length;
            }
        }

        void saveObject(BinaryIO stream, ArchiveObject obj, IsRoot isRoot = IsRoot.no)
        {
            this.saveEntry(stream, DataType.Object, ()
            {
                if(!isRoot)
                    stream.write(obj.name);

                foreach(attrib; obj.attributes)
                    this.saveAttribute(stream, attrib);

                foreach(value; obj.values)
                    this.saveValue(stream, value);

                foreach(child; obj.children)
                    this.saveObject(stream, child);
            }, WriteLength.yes, isRoot);
        }

        void saveAttribute(BinaryIO stream, ArchiveObject.Attribute attrib)
        {
            this.saveEntry(stream, DataType.Attribute, ()
            {
                stream.write(attrib.name);
                this.saveValue(stream, attrib.value);
            }, WriteLength.no);
        }

        void saveValue(BinaryIO stream, ArchiveValue value)
        {
            auto type = this.getTypeFor(value);
            this.saveEntry(stream, type, ()
            {
                assert(type != DataType.Object);
                assert(type != DataType.Attribute);
                final switch(type) with(DataType)
                {
                    case Object:
                    case Attribute:          
                    case Null: break;
                    case Bool:          stream.write(cast(byte)value.get!bool); break;
                    case UByteArray:    stream.writeBytes(value.get!(ubyte[])); break;
                    case Byte:          stream.write(value.get!byte); break;
                    case UByte:         stream.write(value.get!ubyte); break;
                    case Short:         stream.write(value.get!short); break;
                    case UShort:        stream.write(value.get!ushort); break;
                    case Int:           stream.write(value.get!int); break;
                    case UInt:          stream.write(value.get!uint); break;
                    case Long:          stream.write(value.get!long); break;
                    case ULong:         stream.write(value.get!ulong); break;
                    case String:        stream.write(value.get!string); break;
                    case Float:         stream.write(value.get!float); break;
                    case Double:        stream.write(value.get!double); break;

                    case ValueArray: 
                        auto arr = value.get!(ArchiveValue[]);
                        stream.writeLengthBytes(arr.length);

                        foreach(val; arr)
                            this.saveValue(stream, val);
                        break;
                }
            }, WriteLength.no);
        }
    }
}
///
version(Jasterialise_Unittests)
unittest
{
    import fluent.asserts;

    static struct B
    {
        @MainValue
        bool toBOrNotToB;
    }

    static struct A
    {
        @Attribute
        string name;

        @MainValue
        int age;

        B[] muhB;
    }

    auto archive = new ArchiveBinary();
    Serialiser.serialise(A("Bob Ross", 60, [B(false), B(true)]), archive.root);

    ubyte[] data = 
    [
        // Header
        cast(ubyte)'J', 'S', 'E',
        0xEB, 0x3C, 0x4F, 0xC1, // CRC

        // Root object header
        0x00, 0x00, 0x00, 0x2E,

        // Object Entry
        0xFE,
        0x00, 0x00, 0x00, 0x29,
        0x01, 'A',

            // Attribute Entry
            0xFF,
            0x04, 'n', 'a', 'm', 'e',

                // String Entry
                0x0C,
                0x08, 'B', 'o', 'b', ' ', 'R', 'o', 's', 's',

            // Int Entry
            0x08,
            0x00, 0x00, 0x00, 0x3C,

            // Object Entry
            0xFE,
            0x00, 0x00, 0x00, 0x04,
            0x01, 'B',

                // Bool Entry
                0x00,
                0x00,

            // Object Entry
            0xFE,
            0x00, 0x00, 0x00, 0x04,
            0x01, 'B',

                // Bool Entry
                0x00,
                0x01
    ];
    archive.saveToMemory().should.equal(data);

    // Here for manual debugging, and to retrieve the CRC for when the format changes.
    // import std.file;
    // write("Test.bin", archive.saveToMemory());

    archive.loadFromMemory(data);
    auto root = archive.root;

    root["A"].expectAttributeAs!string("name").should.equal("Bob Ross");
    root["A"].children[0].name.should.equal("B");
    root["A"].children[0].getValueAs!bool(0).should.equal(false);
    root["A"].children[1].getValueAs!bool(0).should.equal(true);
}