module jaster.serialise.generator.core;

private
{    
    import std.format, std.traits, std.range, std.typecons, std.algorithm, std.array;
    import jaster.serialise;
}

public import std.typecons : Nullable;

enum TypeFlags : ubyte
{
    None        = 0,
    IsMainValue = 1 << 0,
    IsAttribute = 1 << 1,
    IsOptional  = 1 << 2
}

struct TypeField
{
    string name;
    TypeSerialiseInfo info;

    T as(T)()
    {
        return cast(T)info;
    }
}

abstract class TypeSerialiseInfo
{
    TypeFlags flags;
    Serialiser.Settings settings;
    Serialiser.Settings inheritedSettings;

    @property
    Serialiser.Settings allSettings()
    {
        return (this.settings | this.inheritedSettings);
    }

    string toPrettyString(int tab = 0, string typeName = "Generic")
    {
        auto tabs = ['\t'].cycle.take(tab).array;
        return format(
             "%s[%s]\n"
            ~"%sFlags:             %s\n"
            ~"%sSettings:          %s\n"
            ~"%sInheritedSettings: %s\n",
            tabs, typeName,
            tabs, this.flags,
            tabs, this.settings,
            tabs, this.inheritedSettings
        );
    }
}

class BasicSerialiseInfo : TypeSerialiseInfo
{
    TypeInfo type;

    static BasicSerialiseInfo make(T)()
    if(ArchiveValue.allowed!T)
    {
        auto info = new BasicSerialiseInfo();
        info.type = typeid(T);

        return info;
    }

    override string toPrettyString(int tab = 0, string typeName = "Basic Type")
    {
        auto tabs = ['\t'].cycle.take(tab).array;
        return format(
             "%s"
            ~"%s%sTypeName: %s\n",
            super.toPrettyString(tab + 1, "Basic Type"),
            tabs, (tab > 0 ? '\t' : '\0'), this.type.toString()
        );
    }
}

class ArraySerialiseInfo : TypeSerialiseInfo
{
    string            arrayAsObjectName;
    TypeSerialiseInfo type;
    Nullable!uint     staticArraySize;

    this(TypeSerialiseInfo info)
    {
        this.type = info;
    }

    override string toPrettyString(int tab = 0, string typeName = "Array")
    {
        import std.conv : to;

        auto tabs     = ['\t'].cycle.take(tab).array;
        auto extraTab = (tab > 0 ? '\t' : '\0');
        return format(
             "%s"
            ~"%s%sStaticSize: %s\n"
            ~"%s%sValueType: \n%s\n",
            super.toPrettyString(tab + 1, "Array"),
            tabs, extraTab, (this.staticArraySize.isNull) ? "null" : this.staticArraySize.get.to!string,
            tabs, extraTab, this.type.toPrettyString(tab + 1)
        );
    }
}

class StructSerialiseInfo : TypeSerialiseInfo
{
    TypeField[string] fields;
    TypeInfo structInfo;

    static StructSerialiseInfo make(T)()
    if(is(T == struct))
    {
        auto info = new StructSerialiseInfo();
        info.structInfo = typeid(T);
        return info;
    }

    override string toPrettyString(int tab = 0, string typeName = "Struct")
    {
        auto tabs = ['\t'].cycle.take(tab).array;
        return format(
             "%s"
            ~"%s%sFields: \n%s\n",
            super.toPrettyString(tab + 1, "Struct"),
            tabs, (tab > 0 ? '\t' : '\0'),
            this.fields.byKeyValue
                       .map!(p => format("%sFieldName: %s\n", tabs, p.key) ~ p.value.info.toPrettyString(tab + 1))
                       .joiner("\n")
        );
    }
}

class EnumSerialiseInfo : TypeSerialiseInfo
{
    struct Pair
    {
        string name;
        long   value;
    }

    /// Always an integral type.
    TypeInfo type;
    TypeInfo enumType;
    Pair[] values;

    static EnumSerialiseInfo make(T)()
    if(is(T == enum))
    {
        import std.conv : to;

        alias Type = OriginalType!T;
        static assert(isIntegral!Type, "Only integral enum types are supported, since that's only what most languages support.");

        auto info     = new EnumSerialiseInfo();
        info.type     = typeid(Type);
        info.enumType = typeid(T);

        foreach(member; __traits(allMembers, T))
            info.values ~= Pair(member.to!string, cast(long)mixin("T."~member));

        return info;
    }

    override string toPrettyString(int tab = 0, string typeName = "Enum")
    {
        auto tabs = ['\t'].cycle.take(tab).array;
        return format(
             "%s"
            ~"%s%sValues: \n%s\n",
            super.toPrettyString(tab + 1, "Enum"),
            tabs, (tab > 0 ? '\t' : '\0'),
            this.values.map!(v => format("%s\t%s = %s", tabs, v.name, v.value))
                       .joiner("\n")
        );
    }
}

class TypeIntrospect
{
    public static TypeField getInfoFor(T)()
    if(is(T == struct))
    //out(i; i !is null)
    {
        return TypeField(getFieldName!T, getOrIntrospect!(T, T, T, Serialiser.Settings.None)());
    }

    private static
    {
        TypeSerialiseInfo[TypeInfo] _infoCache;
        TypeSerialiseInfo getOrIntrospect(T, alias MainSymbol, alias Symbol, Serialiser.Settings InheritedSettings)()
        {
            auto ptr = (typeid(T) in _infoCache);
            if(ptr !is null)
                return *ptr;

            auto info = introspect!(T, MainSymbol, Symbol, InheritedSettings);
            _infoCache[typeid(T)] = info;
            return info;
        }

        BasicSerialiseInfo introspect(T, alias MainSymbol, alias Symbol, Serialiser.Settings InheritedSettings)()
        if(ArchiveValue.allowed!T)
        {
            auto info = BasicSerialiseInfo.make!T();
            static if(hasUDA!(Symbol, MainValue))
                info.flags |= TypeFlags.IsMainValue;
            else static if(hasUDA!(Symbol, Attribute))
                info.flags |= TypeFlags.IsAttribute;

            info.settings = getSettings!Symbol;
            info.inheritedSettings = InheritedSettings;

            return info;
        }

        ArraySerialiseInfo introspect(T, alias MainSymbol, alias Symbol, Serialiser.Settings InheritedSettings)()
        if(isArray!T && !isSomeString!T)
        {        
            static if(hasUDA!(Symbol, InheritSettings))
                enum ToInherit = InheritedSettings | getSettings!Symbol;
            else
                enum ToInherit = InheritedSettings;

            enum settings = getSettings!MainSymbol | getSettings!Symbol | InheritedSettings;
            
            auto info = new ArraySerialiseInfo(null);
            info.settings = getSettings!Symbol;
            info.inheritedSettings = InheritedSettings;

            static if(isStaticArray!T)
                info.staticArraySize = T.length;

            static if(ArchiveValue.allowed!(ElementType!T))
            {
                static assert(!(settings & Serialiser.Settings.ArrayAsObject), "The settings 'ArrayAsObject' can only be applied to arrays of structs.");

                static if(hasUDA!(Symbol, MainValue))
                    info.flags |= TypeFlags.IsMainValue;
                
                info.type = getOrIntrospect!(ElementType!T, MainSymbol, Symbol, ToInherit);
            }
            else static if(is(ElementType!T == struct))
            {
                static assert(!hasUDA!(Symbol, MainValue), "Arrays of structs cannot be the main value, they can only be children.");

                static if(settings & Serialiser.Settings.ArrayAsObject)
                    info.arrayAsObjectName = getFieldName!(Symbol, UseArrayBaseType.no);

                info.type = getOrIntrospect!(ElementType!T, MainSymbol, Symbol, ToInherit);
            }
            else static assert(false, "Unsupported array value type: " ~ T.stringof);

            return info;
        }

        EnumSerialiseInfo introspect(T, alias MainSymbol, alias Symbol, Serialiser.Settings InheritedSettings)()
        if(is(T == enum))
        {
            enum settings = getSettings!Symbol;
            auto info = EnumSerialiseInfo.make!T;
            info.settings = settings;
            info.inheritedSettings = InheritedSettings;

            static if(hasUDA!(Symbol, Attribute))
                info.flags |= TypeFlags.IsAttribute;

            static assert(!hasUDA!(Symbol, MainValue), "MainValue enums are not supported right now. (only due to laziness)");

            return info;
        }

        StructSerialiseInfo introspect(T, alias MainSymbol, alias Symbol, Serialiser.Settings InheritedSettings)()
        if(is(T == struct))
        {        
            static assert(getSymbolsByUDA!(T, MainValue).length < 2, "There can only be one field marked with @MainValue");

            auto info = StructSerialiseInfo.make!T;
            info.settings = getSettings!Symbol;
            info.inheritedSettings = InheritedSettings;
            foreach(fieldName; FieldNameTuple!T)
            {
                static if(isPublic!(T, fieldName)
                        && !hasUDA!(mixin("T."~fieldName), Ignore))
                {
                    mixin("alias FieldAlias = T.%s;".format(fieldName));

                    static if(isInstanceOf!(Nullable, typeof(FieldAlias)))
                        alias FieldType = Unqual!(ReturnType!(FieldAlias.get));
                    else
                        alias FieldType = typeof(FieldAlias);

                    static if(hasUDA!(Symbol, InheritSettings))
                        enum ToInherit = InheritedSettings | getSettings!Symbol;
                    else
                        enum ToInherit = InheritedSettings;
                        
                    info.fields[fieldName] = TypeField(fieldName, introspect!(FieldType, MainSymbol, FieldAlias, ToInherit)());
                    static if(isInstanceOf!(Nullable, typeof(FieldAlias)))
                        info.flags |= TypeFlags.IsOptional;
                }
            }
            return info;
        }
    }
}
///
version(Jasterialise_Unittests)
unittest
{
    import fluent.asserts;

    struct A
    {
        @MainValue
        int b;
    }
    
    auto info = TypeIntrospect.getInfoFor!A;
    info.name.should.equal("A");
    info.as!StructSerialiseInfo.fields.length.should.equal(1);

    BasicSerialiseInfo field = info.as!StructSerialiseInfo.fields["b"].as!BasicSerialiseInfo;
    field.should.not.beNull();
    field.type.should.equal(typeid(int));
    field.flags.should.equal(TypeFlags.IsMainValue);
}

// Best I can do at least...
private enum isPublic(T, string field) = is(typeof({T t = T.init; auto b = mixin("t."~field);}));