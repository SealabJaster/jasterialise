module jaster.serialise.generator.csharp;

private
{
    import std.file, std.path, std.format, std.meta, std.traits, std.algorithm;
    import jaster.serialise;

    struct FileInfo
    {
        string fileName;
        string contents;
    }

    FileInfo[] PREMADE_FILES;
    const FRAMEWORK_PATH = "languages/c#/framework/Framework/";
    static this()
    {
        import std.path : baseName;

        static foreach(file;
            [
                "Archive.cs",
                "ArchiveBinary.cs",
                "ArchiveObject.cs",
                "ArchiveValue.cs",
                "BinaryStream.cs",
                "CRC32.cs",
                "Serialiser.cs"
            ]
        )
        {{
            const Data = import(FRAMEWORK_PATH ~ file);
            PREMADE_FILES ~= FileInfo(file.baseName, Data);
        }}
    }
}

class CSharpGenerator
{
    private static
    {
        enum LockType
        {
            Basic,
            Struct,
            Array,
            Enum
        }

        struct LockInfo
        {
            LockType type;
            Object   info;
        }

        bool[LockInfo] _genLocks;
    }

    public static
    {
        void genFilesForTypes(T...)(string outputDir, string namespace)
        if(allSatisfy!(isType, T))
        {
            CSharpGenerator._genLocks.clear();

            TypeField[] info;
            foreach(type; T)
                info ~= TypeIntrospect.getInfoFor!type;

            outputDir = buildNormalizedPath(outputDir);
            if(!exists(outputDir))
                mkdirRecurse(outputDir);

            TypeField[] allInfo; // Some TypeFields are only created *during* the generation process.
            CSharpGenerator.copyPremadeFiles(outputDir, namespace);
            foreach(inf; info)
                CSharpGenerator.genFileFromInfo(outputDir, namespace, inf, allInfo);

            CSharpGenerator.genRegisterFile(outputDir, namespace, allInfo);
        }
    }

    private static
    {
        string getBasicTypeName(BasicSerialiseInfo info)
        {
            return getBasicTypeName(info.type);
        }

        string getBasicTypeName(TypeInfo info)
        {
            assert(info != typeid(null)
                && info != typeid(ubyte[])
                && info != typeid(ArchiveValue[]),
                "Not supported (yet, if ever)"
            );

            return (info == typeid(string)) 
                    ? "string"
                    : (info == typeid(byte))
                        ? "sbyte"
                        : (info == typeid(ubyte))
                            ? "byte"
                            : info.toString();
        }

        string getStructClassName(StructSerialiseInfo info)
        {
            auto split = info.structInfo.toString().splitter(".");
            
            string last;
            foreach(thing; split)
                last = thing;

            return last;
        }

        string getArrayTypeName(ArraySerialiseInfo info)
        {
            return info.type.castSwitch!(
                (BasicSerialiseInfo i)  => "List<"~CSharpGenerator.getBasicTypeName(i)~">",
                (StructSerialiseInfo i) => "List<"~CSharpGenerator.getStructClassName(i)~">",
                (ArraySerialiseInfo i)  => "List<"~CSharpGenerator.getArrayTypeName(i)~">",
                (EnumSerialiseInfo i)   => "List<"~CSharpGenerator.getEnumClassName(i)~">",
            );
        }

        string getArrayClassName(ArraySerialiseInfo info)
        {
            import std.array : replace;

            return CSharpGenerator.getArrayTypeName(info).replace("<", "_").replace(">", "_");
        }

        string getEnumClassName(EnumSerialiseInfo info)
        {
            auto split = info.enumType.toString().splitter(".");
            
            string last;
            foreach(thing; split)
                last = thing;

            return last;
        }

        string getClassName(TypeSerialiseInfo info)
        {
            return info.castSwitch!(
                (BasicSerialiseInfo i)  => CSharpGenerator.getBasicTypeName(i),
                (StructSerialiseInfo i) => CSharpGenerator.getStructClassName(i),
                (ArraySerialiseInfo i)  => CSharpGenerator.getArrayTypeName(i),
                (EnumSerialiseInfo i)   => CSharpGenerator.getEnumClassName(i),
                (Object _)                 {assert(false, info.toPrettyString());}
            );
        }

        void ensureGen(string outputDir, string namespace, TypeField info, ref TypeField[] allInfo)
        {
            if(!CSharpGenerator.isLocked(info.info))
                CSharpGenerator.genFileFromInfo(outputDir, namespace, info, allInfo);
        }

        LockInfo getLockInfo(TypeSerialiseInfo info)
        {
            return info.castSwitch!(
                (BasicSerialiseInfo i)  => LockInfo(LockType.Basic, i.type),
                (StructSerialiseInfo i) => LockInfo(LockType.Struct, i.structInfo),
                (ArraySerialiseInfo i)  => LockInfo(LockType.Array, i.type),
                (EnumSerialiseInfo i)   => LockInfo(LockType.Enum, i.type),
                (Object _)                 {assert(false, info.toPrettyString());}
            );
        }

        void lock(TypeSerialiseInfo info)
        {
            CSharpGenerator._genLocks[CSharpGenerator.getLockInfo(info)] = true;
        }

        bool isLocked(TypeSerialiseInfo info)
        {
            return (CSharpGenerator.getLockInfo(info) in CSharpGenerator._genLocks) !is null;
        }

        void copyPremadeFiles(string outputDir, string namespace)
        {
            import std.array : replace;

            foreach(file; PREMADE_FILES)
            {
                auto path = buildPath(outputDir, file.fileName);
                write(path, file.contents.replace("namespace Framework", "namespace "~namespace));
            }
        }

        string genChildInfoString(TypeField type)
        {
            import std.algorithm : joiner;
            import std.array     : array;
            import std.format    : format;

            string[] flags;
            if(type.info.flags & TypeFlags.IsAttribute)
                flags ~= "TypeFlags.IsAttribute";
            if(type.info.flags & TypeFlags.IsMainValue)
                flags ~= "TypeFlags.IsMainValue";
            if(type.info.allSettings & Serialiser.Settings.ArrayAsObject)
                flags ~= "TypeFlags.ArrayAsObject";
            if(type.info.allSettings & Serialiser.Settings.EnumAsValue)
                flags ~= "TypeFlags.EnumAsValue";
            
            return format("new TypeChildInfo(){ Name = \"%s\", Flags = %s }", type.name, (flags is null) ? "TypeFlags.None" : flags.joiner(" | ").array);
        }

        void genRegisterFile(string outputDir, string namespace, TypeField[] info)
        {
            auto code = new CodeBuilder();

            code.put("using System;");
            code.put("using System.Collections.Generic;");
            code.putf("namespace %s", namespace);
            code.putScope((_)
            {
                code.put("public static class Jasterialise");
                code.putScope((_)
                {
                    code.put("public static void RegisterSerialisers()");
                    code.putScope((_)
                    {
                        foreach(inf; info)
                        {
                            auto className = CSharpGenerator.getClassName(inf.info);
                            auto serialName = (inf.as!ArraySerialiseInfo) ? CSharpGenerator.getArrayClassName(inf.as!ArraySerialiseInfo)
                                                                          : CSharpGenerator.getClassName(inf.info);
                            code.putf("Serialiser.Register<%s>(new %sSerialiser());",
                                className,
                                serialName
                            );
                        }
                    });
                });
            });

            import std.conv : to;
            write(buildPath(outputDir, "Jasterialise.cs"), code.data.to!string);
        }

        void genFileFromInfo(string outputDir, string namespace, TypeField info, ref TypeField[] allInfo)
        {
            auto code = new CodeBuilder();
            string name = (info.as!ArraySerialiseInfo) ? CSharpGenerator.getArrayClassName(info.as!ArraySerialiseInfo)
                                                       : CSharpGenerator.getClassName(info.info);

            allInfo ~= info;
            CSharpGenerator.lock(info.info);

            code.put("using System;");
            code.put("using System.Collections.Generic;");
            code.put("using System.Linq;");
            code.putf("namespace %s", namespace);
            code.putScope((_)
            {
                // "info.info" is a "TypeSerialiseInfo", which is a base class for all the switches below.
                info.info.castSwitch!(
                    (BasicSerialiseInfo _)  => CSharpGenerator.genBasicCode(code, info),
                    (ArraySerialiseInfo _)  => CSharpGenerator.genArrayCode(outputDir, namespace, code, info, allInfo),
                    (StructSerialiseInfo _) => CSharpGenerator.genStructCode(outputDir, namespace, code, info, allInfo),
                    (EnumSerialiseInfo _)   => CSharpGenerator.genEnumCode(outputDir, namespace, code, info, allInfo),
                    (Object _)                 {assert(false, info.info.toPrettyString());}
                );
            });

            import std.conv;
            write(buildPath(outputDir, name~".cs"), code.data.to!string);
        }

        void genEnumCode(string outputDir, string namespace, CodeBuilder code, TypeField typeField, ref TypeField[] allInfo)
        {
            auto info = typeField.as!EnumSerialiseInfo;
            assert(info !is null);

            auto className = CSharpGenerator.getEnumClassName(info);
            auto typeName  = CSharpGenerator.getBasicTypeName(info.type);

            auto basicInfo = new BasicSerialiseInfo();
            basicInfo.type = info.type;
            CSharpGenerator.ensureGen(outputDir, namespace, TypeField(typeName, basicInfo), allInfo);

            basicInfo.type = typeid(string);
            CSharpGenerator.ensureGen(outputDir, namespace, TypeField("string", basicInfo), allInfo);

            code.putf("public enum %s : %s", className, typeName);
            code.putScope((_)
            {
                import std.format;
                code.putf("%s", info.values.map!(v => format("%s = %s", v.name, v.value)).joiner(",\n\t\t"));
            });

            code.putf("public class %sSerialiser : ITypeSerialiser", className);
            code.putScope((_)
            {
                code.put("public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    code.putf("var value = (%s)obj;", className);
                    code.putf("var typeInfo = %s;", CSharpGenerator.genChildInfoString(typeField));
                    code.put("if(info.Flags.HasFlag(TypeFlags.EnumAsValue))");
                        code.putf("\tSerialiser.Serialise(parent, (%s)value, typeInfo);", typeName);
                    code.put("else");
                        code.put("\tSerialiser.Serialise(parent, value.ToString(), typeInfo);");
                });

                code.put("public Object Deserialise(ArchiveObject obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    code.putf("var typeInfo = %s;", CSharpGenerator.genChildInfoString(typeField));
                    code.put("if(info.Flags.HasFlag(TypeFlags.EnumAsValue))");
                        code.putf("\treturn (%s)Serialiser.Deserialise<%s>(obj, typeInfo);", className, typeName);
                    code.put("else");
                        code.putf("\treturn (%s)Enum.Parse(typeof(%s), Serialiser.Deserialise<string>(obj, typeInfo));", className, className);
                });
            });
        }

        void genBasicCode(CodeBuilder code, TypeField typeField)
        {
            auto info = typeField.as!BasicSerialiseInfo;
            assert(info !is null);

            auto typeName = CSharpGenerator.getBasicTypeName(info);
            code.putf("public class %sSerialiser : ITypeSerialiser", typeName);
            code.putScope((_)
            {
                code.put("public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    code.put("if(info.Flags.HasFlag(TypeFlags.IsMainValue))");
                        code.putf("\tparent.AddValueAs<%s>((%s)obj);", typeName, typeName);
                    code.put("else if(info.Flags.HasFlag(TypeFlags.IsAttribute))");
                        code.putf("\tparent.SetAttributeAs<%s>(info.Name ?? \"NAME ME\", (%s)obj);", typeName, typeName);
                    code.put("else");
                    code.putScope((_)
                    {
                        code.putf("var arc = new ArchiveObject(info.Name ?? \"NAME ME\");");
                        code.putf("arc.AddValueAs<%s>((%s)obj);", typeName, typeName);
                        code.put("parent.AddChild(arc);");
                    });
                });

                code.put("public Object Deserialise(ArchiveObject obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    code.put("if(info.Flags.HasFlag(TypeFlags.IsMainValue))");
                        code.putf("\treturn obj.ExpectValueAs<%s>(0);", typeName);
                    code.put("else if(info.Flags.HasFlag(TypeFlags.IsAttribute))");
                        code.putf("\treturn obj.ExpectAttributeAs<%s>(info.Name ?? \"NAME ME\");", typeName);
                    code.put("else");
                        code.putf("\treturn obj.ExpectChild(info.Name ?? \"NAME ME\").ExpectValueAs<%s>(0);", typeName);
                });
            });
        }

        void genArrayCode(string outputDir, string namespace, CodeBuilder code, TypeField typeField, ref TypeField[] allInfo)
        {
            ArraySerialiseInfo info = typeField.as!ArraySerialiseInfo;
            code.putf("public class %sSerialiser : ITypeSerialiser", CSharpGenerator.getArrayClassName(info));
            code.putScope((_)
            {
                code.put("public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    CSharpGenerator.ensureGen(outputDir, namespace, TypeField(CSharpGenerator.getClassName(info.type), info.type), allInfo);
                    code.put("ArchiveObject arc;");
                    if(cast(BasicSerialiseInfo)info.type)
                    {
                        code.put("if(!info.Flags.HasFlag(TypeFlags.IsMainValue))");
                        code.putScope((_)
                        {
                            code.put("arc = new ArchiveObject(info.Name ?? \"NAME ME\");");
                            code.put("parent.AddChild(arc);");
                        });
                        code.put("else");
                            code.put("\tarc = parent;");

                        code.putf("foreach(var val in (%s)obj)", CSharpGenerator.getArrayTypeName(info));
                        code.putScope((_)
                        {
                            code.put("arc.AddValueAs(val);");
                        });
                    }
                    else if(cast(StructSerialiseInfo)info.type)
                    {
                        code.put("if(info.Flags.HasFlag(TypeFlags.ArrayAsObject))");
                        code.putScope((_)
                        {
                            code.put("arc = new ArchiveObject(info.Name ?? \"NAME ME\");");
                            code.put("parent.AddChild(arc);");
                        });
                        code.put("else");
                            code.put("\tarc = parent;");

                        code.putf("foreach(var val in (%s)obj)", CSharpGenerator.getArrayTypeName(info));
                        code.putScope((_)
                        {
                            code.put("Serialiser.Serialise(arc, val, new TypeChildInfo());");
                        });
                    }
                    else if(cast(ArraySerialiseInfo)info.type)
                        assert(false, "Arrays of arrays are not supported");
                    else
                        assert(false, info.toPrettyString());
                });

                code.put("public Object Deserialise(ArchiveObject obj, TypeChildInfo info)");
                code.putScope((_)
                {
                    code.put("ArchiveObject arc;");
                    code.putf("var value = new %s();", CSharpGenerator.getArrayTypeName(info));
                    if(cast(BasicSerialiseInfo)info.type)
                    {
                        auto valueType = cast(BasicSerialiseInfo)info.type;
                        code.put("if(!info.Flags.HasFlag(TypeFlags.IsMainValue))");
                            code.put("\tarc = obj.ExpectChild(info.Name ?? \"NAME ME\");");
                        code.put("else");
                            code.put("\tarc = obj;");

                        code.put("foreach(var val in arc.Values)");
                        code.putScope((_)
                        {
                            code.putf("value.Add(val.Get<%s>());", CSharpGenerator.getBasicTypeName(valueType));
                        });
                    }
                    else if(cast(StructSerialiseInfo)info.type)
                    {
                        auto valueType = cast(StructSerialiseInfo)info.type;
                        code.put("if(info.Flags.HasFlag(TypeFlags.ArrayAsObject))");
                            code.put("\tarc = obj.ExpectChild(info.Name ?? \"NAME ME\");");
                        code.put("else");
                            code.put("\tarc = obj;");

                        code.putf("foreach(var val in arc.Children.Where(c => c.Name == \"%s\"))", CSharpGenerator.getStructClassName(valueType));
                        code.putScope((_)
                        {
                            code.putf("value.Add(Serialiser.Deserialise<%s>(val, new TypeChildInfo()));", CSharpGenerator.getStructClassName(valueType));
                        });
                    }
                    else if(cast(ArraySerialiseInfo)info.type)
                        assert(false, "Arrays of arrays are not supported");
                    else
                        assert(false);
                    code.put("return value;");
                });
            });
        }

        void genStructCode(string outputDir, string namespace, CodeBuilder code, TypeField typeField, ref TypeField[] allInfo)
        {
            CSharpGenerator.genStructClass(outputDir, namespace, code, typeField, allInfo);
            CSharpGenerator.genStructSerialiser(code, typeField);
        }

        void genStructClass(string outputDir, string namespace, CodeBuilder code, TypeField typeField, ref TypeField[] allInfo)
        {
            auto info = typeField.as!StructSerialiseInfo;

            code.putf("public class %s", CSharpGenerator.getStructClassName(info));
            code.putScope((_)
            {
                code.putf("public %s()", CSharpGenerator.getStructClassName(info));
                code.putScope((_)
                {
                    foreach(name, field; info.fields)
                    {
                        CSharpGenerator.ensureGen(outputDir, namespace, field, allInfo);
                        if(field.as!ArraySerialiseInfo)
                            code.putf("this.%s = new %s();", name, CSharpGenerator.getArrayTypeName(field.as!ArraySerialiseInfo));
                    }
                });

                foreach(name, field; info.fields)
                {
                    CSharpGenerator.ensureGen(outputDir, namespace, field, allInfo);
                    code.putf("public %s %s;", CSharpGenerator.getClassName(field.info), name);
                }
            });
        }

        void genStructSerialiser(CodeBuilder code, TypeField typeField)
        {
            auto info = typeField.as!StructSerialiseInfo;

            code.putf("public class %sSerialiser : ITypeSerialiser", CSharpGenerator.getStructClassName(info));
            code.putScope((_)
            {
                CSharpGenerator.genStructSerialiseFunc(code, info, typeField.name);
                CSharpGenerator.genStructDeserialiseFunc(code, info, typeField.name);
            });
        }

        void genStructSerialiseFunc(CodeBuilder code, StructSerialiseInfo info, string name)
        {
            code.put("public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)");
            code.putScope((_)
            {
                code.putf("var retObj = new ArchiveObject(info.Name ?? \"%s\");", name);
                code.putf("var value = (%s)obj;", CSharpGenerator.getStructClassName(info));
                foreach(fieldName, fieldType; info.fields)
                {
                    // Static arrays aren't so 'static' in C#
                    if(fieldType.as!ArraySerialiseInfo)
                    {
                        auto array = fieldType.as!ArraySerialiseInfo;
                        if(!array.staticArraySize.isNull)
                        {
                            code.putf("if(value.%s.Count != %s)", fieldName, array.staticArraySize);
                            code.putf("\tthrow new Exception($\"The field '%s' must have exactly %s values, not {value.%s.Count}\");",
                                fieldName,
                                array.staticArraySize,
                                fieldName
                            );
                        }
                    }

                    code.putf("Serialiser.Serialise(retObj, value.%s, %s);", fieldName, CSharpGenerator.genChildInfoString(fieldType));
                }
                code.put("parent.AddChild(retObj);");
            });
        }

        void genStructDeserialiseFunc(CodeBuilder code, StructSerialiseInfo info, string name)
        {
            code.put("public Object Deserialise(ArchiveObject obj, TypeChildInfo info)");
            code.putScope((_)
            {
                code.putf("if(obj.Name != (info.Name ?? \"%s\"))", name);
                code.putf("\tobj = obj.ExpectChild(info.Name ?? \"%s\");", name);
                code.putf("var value = new %s();", CSharpGenerator.getStructClassName(info));
                foreach(fieldName, fieldType; info.fields)
                {
                    string typeName = CSharpGenerator.getClassName(fieldType.info);
                    code.putf("value.%s = Serialiser.Deserialise<%s>(obj, %s);", fieldName, typeName, CSharpGenerator.genChildInfoString(fieldType));

                    // Static arrays aren't so 'static' in C#
                    if(fieldType.as!ArraySerialiseInfo)
                    {
                        auto array = fieldType.as!ArraySerialiseInfo;
                        if(!array.staticArraySize.isNull)
                        {
                            code.putf("if(value.%s.Count != %s)", fieldName, array.staticArraySize);
                            code.putf("\tthrow new Exception($\"The field '%s' expects to have exactly %s values, not {value.%s.Count}\");",
                                fieldName,
                                array.staticArraySize,
                                fieldName
                            );
                        }
                    }
                }
                code.put("return value;");
            });
        }
    }
}
unittest
{
    enum E
    {
        A = 1,
        B = 1,
        C = 69
    }

    @Name("sprite")
    struct SpriteData
    {
        @MainValue
        string name;

        int[2] position;
        int[2] size;
    }

    @Name("spriteSheet")
    struct SpriteSheetData
    {
        @MainValue
        string name;

        int[2] position;
        int[2] size;
        int[2] frameSize;
    }

    @Name("Sprite:atlas")
    struct AtlasData
    {
        E someE;
        string name;
        string textureRef;
        SpriteData[] sprites;
        SpriteSheetData[] spriteSheets;
    }

    // import std.stdio;
    // writeln(TypeIntrospect.getInfoFor!AtlasData.info.toPrettyString);

    CSharpGenerator.genFilesForTypes!AtlasData(buildPath(getcwd(), "test/"), "Test");
}