﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Jasterialise.RegisterSerialisers();

            var archive = new ArchiveBinary();
            Serialiser.Serialise<Assets>(archive.Root, 
                new Assets()
                {
                    textures = new List<Texture>()
                    {
                        new Texture()
                        {
                            key = "Background",
                            file = new File() { type = FileType.Static, path = "img/background.png" }
                        },

                        new Texture()
                        {
                            key = "Player",
                            file = new File() { type = FileType.Dynamic, path = "img/player.png" }
                        }
                    },

                    props = new List<Prop>()
                    {
                        new Prop()
                        {
                            textureKey = "Background",
                            position = new List<int>(){ 0, 0 }
                        },

                        new Prop()
                        {
                            textureKey = "Player",
                            position = new List<int>(){ 200, 400 }
                        }
                    }
                }
            );

            archive.SaveToFile("../../../../output/csharp.bin");
            archive.LoadFromMemory(archive.SaveToMemory());
            Serialiser.Deserialise<Assets>(archive.Root);

            Assert(archive.Root.Children.Count == 1);

            var obj = archive.Root.ExpectChild("Assets");
            Assert(obj.Children.Count == 3);
            
            var texture = obj.ExpectChild("Texture"); // Only bothering to test the first one
            Assert(texture.ExpectChild("key").ExpectValueAs<string>(0) == "Background");
            Assert(texture.ExpectChild("file").ExpectChild("type").ExpectValueAs<string>(0) == "Static");

            Assert(obj.ExpectChild("props").Children.Count == 2);

            var prop = obj.ExpectChild("props").ExpectChild("Prop");
            Assert(prop.ExpectChild("textureKey").ExpectValueAs<string>(0) == "Background");
            Assert(prop.ExpectChild("position").ExpectValueAs<int>(0) == 0);
        }

        static void Assert(bool condition)
        {
            if(!condition)
                throw new Exception();
        }
    }
}
