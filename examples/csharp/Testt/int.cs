using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class intSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			if(info.Flags.HasFlag(TypeFlags.IsMainValue))
				parent.AddValueAs<int>((int)obj);
			else if(info.Flags.HasFlag(TypeFlags.IsAttribute))
				parent.SetAttributeAs<int>(info.Name ?? "NAME ME", (int)obj);
			else
			{
				var arc = new ArchiveObject(info.Name ?? "NAME ME");
				arc.AddValueAs<int>((int)obj);
				parent.AddChild(arc);
			}
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			if(info.Flags.HasFlag(TypeFlags.IsMainValue))
				return obj.ExpectValueAs<int>(0);
			else if(info.Flags.HasFlag(TypeFlags.IsAttribute))
				return obj.ExpectAttributeAs<int>(info.Name ?? "NAME ME");
			else
				return obj.ExpectChild(info.Name ?? "NAME ME").ExpectValueAs<int>(0);
		}
	}
}
