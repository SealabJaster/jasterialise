using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class List_int_Serialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			ArchiveObject arc;
			if(!info.Flags.HasFlag(TypeFlags.IsMainValue))
			{
				arc = new ArchiveObject(info.Name ?? "NAME ME");
				parent.AddChild(arc);
			}
			else
				arc = parent;
			foreach(var val in (List<int>)obj)
			{
				arc.AddValueAs(val);
			}
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			ArchiveObject arc;
			var value = new List<int>();
			if(!info.Flags.HasFlag(TypeFlags.IsMainValue))
				arc = obj.ExpectChild(info.Name ?? "NAME ME");
			else
				arc = obj;
			foreach(var val in arc.Values)
			{
				value.Add(val.Get<int>());
			}
			return value;
		}
	}
}
