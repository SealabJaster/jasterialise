using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class Texture
	{
		public Texture()
		{
		}
		public string key;
		public File file;
	}
	public class TextureSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			var retObj = new ArchiveObject(info.Name ?? "Texture");
			var value = (Texture)obj;
			Serialiser.Serialise(retObj, value.key, new TypeChildInfo(){ Name = "key", Flags = TypeFlags.None });
			Serialiser.Serialise(retObj, value.file, new TypeChildInfo(){ Name = "file", Flags = TypeFlags.None });
			parent.AddChild(retObj);
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			if(obj.Name != (info.Name ?? "Texture"))
				obj = obj.ExpectChild(info.Name ?? "Texture");
			var value = new Texture();
			value.key = Serialiser.Deserialise<string>(obj, new TypeChildInfo(){ Name = "key", Flags = TypeFlags.None });
			value.file = Serialiser.Deserialise<File>(obj, new TypeChildInfo(){ Name = "file", Flags = TypeFlags.None });
			return value;
		}
	}
}
