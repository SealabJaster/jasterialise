using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public enum FileType : int
	{
		Static = 0,
		Dynamic = 1
	}
	public class FileTypeSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			var value = (FileType)obj;
			var typeInfo = new TypeChildInfo(){ Name = "type", Flags = TypeFlags.None };
			if(info.Flags.HasFlag(TypeFlags.EnumAsValue))
				Serialiser.Serialise(parent, (int)value, typeInfo);
			else
				Serialiser.Serialise(parent, value.ToString(), typeInfo);
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			var typeInfo = new TypeChildInfo(){ Name = "type", Flags = TypeFlags.None };
			if(info.Flags.HasFlag(TypeFlags.EnumAsValue))
				return (FileType)Serialiser.Deserialise<int>(obj, typeInfo);
			else
				return (FileType)Enum.Parse(typeof(FileType), Serialiser.Deserialise<string>(obj, typeInfo));
		}
	}
}
