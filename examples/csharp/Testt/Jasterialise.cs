using System;
using System.Collections.Generic;
namespace Test
{
	public static class Jasterialise
	{
		public static void RegisterSerialisers()
		{
			Serialiser.Register<Assets>(new AssetsSerialiser());
			Serialiser.Register<List<Prop>>(new List_Prop_Serialiser());
			Serialiser.Register<Prop>(new PropSerialiser());
			Serialiser.Register<List<int>>(new List_int_Serialiser());
			Serialiser.Register<int>(new intSerialiser());
			Serialiser.Register<string>(new stringSerialiser());
			Serialiser.Register<List<Texture>>(new List_Texture_Serialiser());
			Serialiser.Register<Texture>(new TextureSerialiser());
			Serialiser.Register<File>(new FileSerialiser());
			Serialiser.Register<FileType>(new FileTypeSerialiser());
		}
	}
}
