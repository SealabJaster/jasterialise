using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class Assets
	{
		public Assets()
		{
			this.props = new List<Prop>();
			this.textures = new List<Texture>();
		}
		public List<Prop> props;
		public List<Texture> textures;
	}
	public class AssetsSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			var retObj = new ArchiveObject(info.Name ?? "Assets");
			var value = (Assets)obj;
			Serialiser.Serialise(retObj, value.props, new TypeChildInfo(){ Name = "props", Flags = TypeFlags.ArrayAsObject });
			Serialiser.Serialise(retObj, value.textures, new TypeChildInfo(){ Name = "textures", Flags = TypeFlags.None });
			parent.AddChild(retObj);
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			if(obj.Name != (info.Name ?? "Assets"))
				obj = obj.ExpectChild(info.Name ?? "Assets");
			var value = new Assets();
			value.props = Serialiser.Deserialise<List<Prop>>(obj, new TypeChildInfo(){ Name = "props", Flags = TypeFlags.ArrayAsObject });
			value.textures = Serialiser.Deserialise<List<Texture>>(obj, new TypeChildInfo(){ Name = "textures", Flags = TypeFlags.None });
			return value;
		}
	}
}
