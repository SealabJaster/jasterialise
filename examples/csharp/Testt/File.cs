using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class File
	{
		public File()
		{
		}
		public FileType type;
		public string path;
	}
	public class FileSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			var retObj = new ArchiveObject(info.Name ?? "file");
			var value = (File)obj;
			Serialiser.Serialise(retObj, value.type, new TypeChildInfo(){ Name = "type", Flags = TypeFlags.None });
			Serialiser.Serialise(retObj, value.path, new TypeChildInfo(){ Name = "path", Flags = TypeFlags.None });
			parent.AddChild(retObj);
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			if(obj.Name != (info.Name ?? "file"))
				obj = obj.ExpectChild(info.Name ?? "file");
			var value = new File();
			value.type = Serialiser.Deserialise<FileType>(obj, new TypeChildInfo(){ Name = "type", Flags = TypeFlags.None });
			value.path = Serialiser.Deserialise<string>(obj, new TypeChildInfo(){ Name = "path", Flags = TypeFlags.None });
			return value;
		}
	}
}
