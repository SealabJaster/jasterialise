using System;
using System.Collections.Generic;
using System.Linq;
namespace Test
{
	public class Prop
	{
		public Prop()
		{
			this.position = new List<int>();
		}
		public List<int> position;
		public string textureKey;
	}
	public class PropSerialiser : ITypeSerialiser
	{
		public void Serialise(ArchiveObject parent, Object obj, TypeChildInfo info)
		{
			var retObj = new ArchiveObject(info.Name ?? "Prop");
			var value = (Prop)obj;
			if(value.position.Count != 2)
				throw new Exception($"The field 'position' must have exactly 2 values, not {value.position.Count}");
			Serialiser.Serialise(retObj, value.position, new TypeChildInfo(){ Name = "position", Flags = TypeFlags.None });
			Serialiser.Serialise(retObj, value.textureKey, new TypeChildInfo(){ Name = "textureKey", Flags = TypeFlags.None });
			parent.AddChild(retObj);
		}
		public Object Deserialise(ArchiveObject obj, TypeChildInfo info)
		{
			if(obj.Name != (info.Name ?? "Prop"))
				obj = obj.ExpectChild(info.Name ?? "Prop");
			var value = new Prop();
			value.position = Serialiser.Deserialise<List<int>>(obj, new TypeChildInfo(){ Name = "position", Flags = TypeFlags.None });
			if(value.position.Count != 2)
				throw new Exception($"The field 'position' expects to have exactly 2 values, not {value.position.Count}");
			value.textureKey = Serialiser.Deserialise<string>(obj, new TypeChildInfo(){ Name = "textureKey", Flags = TypeFlags.None });
			return value;
		}
	}
}
