import std.stdio;
import jaster.serialise;

enum FileType
{
	Static,
	Dynamic
}

struct File
{
	FileType type;
	string path;
}

struct Texture
{
	string key;
	File file;
}

struct Prop
{
	string textureKey;
	int[2] position;
}

struct Assets
{
	Texture[] textures;

	@Setting(Serialiser.Settings.ArrayAsObject)
	Prop[] props;
}

void main()
{
	CSharpGenerator.genFilesForTypes!Assets("../csharp/Testt/", "Test");

	auto test = Assets(
		[
			Texture("Background", File(FileType.Static, "img/background.png")),
			Texture("Player", File(FileType.Dynamic, "img/player.png"))
		],

		[
			Prop("Background", [0, 0]),
			Prop("Player", [200, 400])
		]
	);

	auto sdl = new ArchiveSDL();
	auto binary = new ArchiveBinary();

	Serialiser.serialise(test, sdl.root);
	Serialiser.serialise(test, binary.root);

	sdl.saveToFile("../output/dlang.sdl");
	binary.saveToFile("../output/dlang.bin");
}
