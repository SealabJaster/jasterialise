# About this library

This is a serialisation library, but the feature putting this library apart from other (and if you're using pure D, arguably better) libraries
is that this library aims to provide code generation features for other languages.

The purpose of this is so you define all the types on the D side, call a generator to generate code for a different language,
and then both languages are setup and ready to use the same data between eachother.

# Disclaimer

This is currently in a 'proof-of-concept' stage. If there's any interest in this library then I'll work to refine it into something
more usable, otherwise it can just be left as is for someone to play around with. There will be bugs, there will be rough edges,
there will be and are unimplemented features (generators can't handle enums yet for example). There isn't any real documentation for the
generation code. Things will also probably feel incomplete, or iffy to use.

Currently, C# is the only language with a generator written for it (from some really basic tests, they produce the same output, so I assume it works
in most cases).

# How it works (Serialisation)

The basic idea is, there are 3 parts to the serialisation process: An Archive, ArchiveObjects, and a Serialiser.

The `ArchiveObject` class is "inspired" from sdlang-d's `Tag` class and is a standard way to store and represent data.

The job of a Serialiser (there is a default serialiser built into this library) is to take a type and convert it into an `ArchiveObject`, or
to convert an `ArchiveObject` into a type. Compile time introspection can be used for detailed serialisation of types.

The job of an Archive is to take in a root `ArchiveObject` and then save that to a file in whatever format.
It can also deserialise from a format into a root `ArchiveObject`.
This library comes with serialisers for SDLang and a custom binary format (on the D side, see below).

So the workflow for serialisation is: Type -> Serialiser -> ArchiveObject -> Archive -> File/Data

Deserialisation is: File/Data -> Archive -> ArchiveObject -> Serialiser -> Type

The seperation of Archives and Serialisers allows archives to write/read data independent of the way the serialisers work.
It also allows different serialisers to be created without needing to rewrite an Archive for each one.

For example, the default serialiser is designed to create output suited for text-based formats that the user will be editing,
however this means it's a bit inefficient as it has to create more data than it needs so it gets layed out properly. This
can be overcome by creating a custom serialiser focused on making things as compact as possible.

(Note: All built-in generators, and parts of the generation code are tailored for the default serialiser. Generators for custom serialisers
likely won't be as comfortable as I'd like.)

# How it works (Generation)

All generators are written in D, and are made for other languages. C# is currently the only supported generator (I repeat that this is a proof of concept right now).

The first thing that has to be done is that the non-generation part of this library (Archive, ArchiveObject, and depending on the implementation, a
'Serialiser' class) need to be ported over to the target language.

After that, the `TypeInstrospect` class can be used to get all the information from a type that is relevent to serialisation. This makes use of
D's compile-time introspection to gather things such as UDAs, type info, etc.

From there, a generator class can take this information and generate code that targets the ported framework for it's target language.

# Examples

Checkout the `examples` folder.

The `examples/d` folder currently generates the files for the csharp project (`examples/csharp`), and also performs
a test serialisation on some data, serialised to SDLang and Binary (in the `examples/output` folder).

The `examples/csharp` folder shows the generated code, and how to make use of it (in `Program.cs`) by performing
the same serialisation that the D project does, also outputting to `examples/output`.

I'd like to note that both the D and C# serialisers **are** producing the same output, it's just that the C#
version is doing it in a different order (which I need to look into), so the files produced aren't exact matches.

# Supported Languages and Archives

|Lang|SDLang|Binary|
|----|:----:|:----:|
|C#  |No    |Yes   |
|D   |Yes   |Yes   |

# Contribution

I'm completely fine with people wanting to help out, whether it simply be pointing out bugs, design flaws, or opening PRs.

Just note that this is my first public project, and I'm a bit of an ameteur, so it might be a bit frustrating discussing things
with someone who barely knows better. I will try my best though.