﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Tests
{
    [TestClass()]
    public class ArchiveBinaryTests
    {
        [TestMethod()]
        public void ArchiveBinaryTest()
        {
            var obj = new ArchiveObject("A");
            obj.SetAttributeAs<string>("name", "Bob Ross");
            obj.AddValueAs<int>(60);
            
            var obj2 = new ArchiveObject("B");
            obj2.AddValueAs<bool>(false);
            obj.AddChild(obj2);

            var obj3 = new ArchiveObject("B");
            obj3.AddValueAs<bool>(true);
            obj.AddChild(obj3);

            var archive = new ArchiveBinary();
            archive.Root.AddChild(obj);
            System.IO.File.WriteAllBytes("Test.bin", archive.SaveToMemory().ToArray());

            archive.LoadFromMemory(archive.SaveToMemory());

            obj  = archive.Root.ExpectChild("A");
            obj2 = obj.Children[0];
            obj3 = obj.Children[1];

            Assert.AreEqual(obj.GetAttributeAs<string>("name"), "Bob Ross");
            Assert.AreEqual(obj.GetValueAs<int>(0), 60);
            Assert.AreEqual(obj2.GetValueAs<bool>(0), false);
            Assert.AreEqual(obj3.GetValueAs<bool>(0), true);
        }
    }
}