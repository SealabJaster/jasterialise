﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Tests
{
    [TestClass()]
    public class ArchiveObjectTests
    {
        [TestMethod()]
        public void ArchiveObjectTest()
        {
            Assert.AreEqual(new ArchiveObject("Daddy").Name, "Daddy");
        }

        [TestMethod()]
        public void AttributeTest()
        {
            var obj = new ArchiveObject();
            obj.SetAttributeAs<int>("Age", 900); // Calls SetAttribute

            Assert.AreEqual(obj.ExpectAttributeAs<int>("Age"), 900); // Calls ExpectAttribute, GetAttribute
            Assert.ThrowsException<IndexOutOfRangeException>(() => obj.ExpectAttribute("boob"));
            Assert.AreEqual(obj.GetAttributeAs<int>("boob", new Lazy<int>(() => 20)), 20);
        }

        [TestMethod()]
        public void ValueTest()
        {
            var obj = new ArchiveObject();
            obj.AddValueAs<int>(900); // Calls SetValue

            Assert.AreEqual(obj.ExpectValueAs<int>(0), 900); // Calls ExpectValue, GetValue
            Assert.ThrowsException<IndexOutOfRangeException>(() => obj.ExpectValue(2000));
            Assert.AreEqual(obj.GetValueAs<int>(2000, new Lazy<int>(() => 20)), 20);
        }

        [TestMethod()]
        public void ChildTest()
        {
            var obj = new ArchiveObject();
            var child = new ArchiveObject("Toddy");
            obj.AddChild(child); // Calls SetChild

            Assert.AreEqual(obj.ExpectChild("Toddy"), child); // Calls ExpectChild, GetChild
            Assert.ThrowsException<IndexOutOfRangeException>(() => obj.ExpectChild("boob"));
            Assert.AreEqual(obj.GetChild("boob"), null);
        }
    }
}