﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Tests
{
    [TestClass()]
    public class ArchiveValueTests
    {
        [TestMethod()]
        public void CreateGetTest()
        {
            var obj = ArchiveValue.Create<int>(69);
            Assert.AreEqual(obj.CurrentType, typeof(int));
            Assert.AreEqual(obj.Get<int>(), 69);
        }

        [TestMethod()]
        public void ExceptionTest()
        {
            Assert.ThrowsException<InvalidArchiveTypeException>(() => ArchiveValue.Create<DateTime>(new DateTime()));
            Assert.ThrowsException<ArrayTypeMismatchException>(() => ArchiveValue.Create<int>(0).Get<string>());
        }
    }
}